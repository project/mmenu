<?php

namespace Drupal\mmenu\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class MmenuSettingsForm to include settings for mmenu.
 *
 * @package Drupal\mmenu\Form
 *
 * @ingroup mmenu
 */
class MmenuSettingsForm extends FormBase {

  use StringTranslationTrait;

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'mmenu_settings';
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['mmenu.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $mmenu_name = '') {

    $this->messenger()->addWarning('Caution: most of settings here are currently disabled. See README');

    $config = $this->config('mmenu.settings');

    // Commenting below line for now
    // Getting Error in settings page for mmenu_list function.
    // $mmenu = mmenu_list($mmenu_name);
    $form['general'] = [
      '#tree' => TRUE,
      '#type' => 'details',
      '#title' => $this->t('General'),
      '#weight' => -5,
      '#open' => TRUE,
    ];

    $form['general']['enabled'] = [
      '#title' => $this->t('Enabled?'),
      '#type' => 'select',
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
      '#default_value' => $config->get('enabled') ?? 0,
      '#required' => TRUE,
      '#weight' => -3,
      '#description' => $this->t('Enable or disable the mmenu.'),
    ];

    $form['general']['name'] = [
      '#type' => 'hidden',
      '#value' => $mmenu_name,
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#weight' => 0,
      '#submit' => ['::saveSettings'],
    ];

    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#weight' => 1,
      '#submit' => ['::resetSettings'],
    ];

    return $form;
  }

  /**
   * Save Form settings.
   */
  public function saveSettings(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Retrieve the configuration and save it.
    $this->configFactory->getEditable('mmenu.settings')
      ->set('enabled', $values['general']['enabled'])
      ->set('mmenu_item_' . $values['general']['name'], $values['general']['name'])
      ->save();

    $this->messenger()->addMessage('The settings have been saved.');
  }

  /**
   * Reset Form settings.
   */
  public function resetSettings(array &$form, FormStateInterface $form_state) {
    // Delete the configuration.
    $this->configFactory->getEditable('mmenu.settings')->delete();

    $this->messenger()->addWarning('The settings have been reset.');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
