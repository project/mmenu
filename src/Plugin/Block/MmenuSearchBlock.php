<?php

namespace Drupal\mmenu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Search block.
 *
 * @Block(
 *   id = "mmenu:search",
 *   admin_label = @Translation("Search")
 * )
 */
class MmenuSearchBlock extends BlockBase {

  /**
   * Form builder will be used via Dependency Injection.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructing Form Builder Interface.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service to load search form.
   */
  public function __construct(FormBuilderInterface $form_builder) {
    $this->formBuilder = $form_builder;
  }

  /**
   * Create Method to get services.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container variable to get services.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->formBuilder->getForm('Drupal\search\Form\SearchBlockForm');
  }

}
