<?php

/**
 * @file
 * Hooks provided by mmenu module.
 */

/**
 * Allows modules to add more mmenus.
 */
function hook_mmenu() {
  return [
    'mmenu_left' => [
      'enabled' => TRUE,
      'enabled_callback' => [
        'php' => [
          'mmenu_enabled_callback',
        ],
        'js' => [
          'mmenu_enabled_callback',
        ],
      ],
      'name' => 'mmenu_left',
      'title' => t('Left menu'),
      'blocks' => [
        [
          'title' => t('Main menu'),
          'module' => 'system',
          'delta' => 'main-menu',
          'collapsed' => FALSE,
          'wrap' => FALSE,
        ],
        [
          'title' => t('Management'),
          'module' => 'system',
          'delta' => 'management',
          'collapsed' => FALSE,
          'wrap' => FALSE,
          'menu_parameters' => [
            'min_depth' => 2,
          ],
        ],
      ],
      'options' => [
        'position' => 'left',
        'classes' => 'mm-light',
      ],
      'configurations' => [],
      // Adds your own CSS or JS handlers if you want.
      'custom' => [
        'library' => ['examples/example.mmenu'],
      ],
    ],
  ];
}

/**
 * Allows modules to alter mmenu settings.
 */
function hook_mmenu_alter(&$mmenus) {
  $mmenus['mmenu_left']['enabled'] = FALSE;
}

/**
 * Allows modules to add more mmenu themes.
 */
function hook_mmenu_theme() {
  return [
    'mm-basic' => [
      'name' => 'mm-basic',
      'title' => t('mm-basic'),
      'library' => ['examples/examples.mm_basic'],
    ],
  ];
}

/**
 * Allows modules to alter mmenu theme settings.
 */
function hook_mmenu_theme_alter(&$classes) {
  $classes['mm-basic']['library'] = ['examples/examples.mm_basic_custom'];
}

/**
 * Allows modules to add more mmenu effects.
 */
function hook_mmenu_effect() {
  return [
    'mm-slide' => [
      'name' => 'mm-slide',
      'title' => t('mm-slide'),
      'library' => ['examples/examples.mm_slide'],
    ],
  ];
}

/**
 * Allows modules to alter mmenu effect settings.
 */
function hook_mmenu_effect_alter(&$effects) {
  $effects['mm-slide']['library'] = ['examples/examples.mm_slide_custom'];
}

/**
 * Allows modules to add more mmenu icon classes.
 */
function hook_mmenu_icon() {
  $icons = [
    'path' => [
      'home' => 'icon-home',
      'about' => 'icon-office',
      'contact' => 'icon-envelope',
    ],
    'block' => [
      [
        'module' => 'system',
        'delta' => 'main-menu',
        'icon_class' => 'icon-enter',
      ],
    ],
  ];
  return $icons;
}

/**
 * Allows modules to alter mmenu icon class settings.
 */
function hook_mmenu_icon_alter(&$icons) {
  $icons['path']['home'] = 'icon-home1';
}
